module Main where

import M1run


main :: IO ()
main =
  do putStrLn "Testing Unmodified M1 circuit \n"
     run_Sigma16_program test_leas 16
     --run_Sigma16_program test_loads 16



test_loads :: [[Char]]
test_loads =
 [
  "f101",
  "000d",         
  "f201",         
  "000e",        
  "f301",
  "000f",         
  "f401",       
  "000d",        
  "f101",    
  "000e",
  "f201",         
  "000f",         
  "d000",         
  "0017",         
  "0063"]

test_leas :: [[Char]]
test_leas =
 [
  "f100",
  "0001",        
  "f200",        
  "0002",
  "f300",        
  "0003",       
  "f400",        
  "0004",       
  "f500",       
  "0005",         
  "d000"]