 module Main where

-- This is a main program for Sigma16, providing a GUI in a web
-- browser, using the Threepenny library to interface with the
-- browser.

-- Run main in this module, and open localhost:8023 in a browser

import GUI

main :: IO ()
main = mainGUI


{-
import Control.Concurrent
import Control.Exception
import System.IO.Error
import Control.Monad
import Control.Monad.State
import Data.IORef
import Data.List
import Data.Char
import Data.Word
import Text.Read
import System.Directory
import System.FilePath
import System.Exit 
import System.IO
import System.IO.Error
import qualified Graphics.UI.Threepenny as UI
import Graphics.UI.Threepenny.Core
import Graphics.UI.Threepenny.Elements

import Arithmetic
import Architecture
import S16Module
import Syntax
import Common
import State
import Assembler
import Linker
import Controller
import Emulator
import SysEnv

-}
