------------------------------------------------------------------------------
Running from source using cygwin and stack

For development it's more convenient to use ghci, but this needs to be
a version of ghci that can run all the libraries, and we need to have
the Sigma16 source files loaded.

On cygwin the cygpath program with option -w converts a path to
windows format (C:\Users\...)

cygpath -w `pwd`

Create the SIGMA16 environment variable with a fixed path.  In Linux,
this can be set in .bashrc and on Windows search for environment
variable and use the control panel.

Run the right version of ghci with the source files loaded

stack setup           if yaml has changed
stack ghci            runs the right version and loads the sources
output goes to term
to kill process ^C

------------------------------------------------------------------------------
Running from source using eshell

stack setup
stack ghci
output goes to eshell
to kill process ^C^K

------------------------------------------------------------------------------

Threepenny on Windows needs ghc-8.0.2 (32 bit)

LTS 9.21 for GHC 8.0.2, published 2018

stack new --resolver lts-9.21 Sigma16

It built new Sigma16 directory, it has:
resolver: lts-9.21
After uncommenting arch, it now has:
arch: i386

stack setup
stack build

Now add dependencies in package.yaml, all except threepenny-gui
Finally add threepenny-gui dependency

stack build
