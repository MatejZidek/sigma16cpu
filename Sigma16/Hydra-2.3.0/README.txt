Hydra: a functional computer hardware description language

For more information, open index.html in a browser
Copyright (c) 2018 John T. O'Donnell
This is free software; see LICENSE.txt

--------  -----------------------------------------------------
author:   John T. O'Donnell
email:    john.odonnell@glasgow.ac.uk
email:    john.t.odonnell9@gmail.com
web:      www.dcs.gla.ac.uk/~jtod/
post:     School of Computing Science, University of Glasgow,
          Glasgow G12 8QQ, United Kingdom
--------  -----------------------------------------------------
