
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity FlashTest is
  port (
    CLK_24M     : in    std_logic; -- A12/B12
    LED_RG      : out   std_logic_vector(15 downto 0); -- R[7]-R[0]+G[7]-G[0]: U18,Y18,V19,T18,Y19,U19,R19,R20, Y21,Y22,W21,W22,V21,V22,U21,U22
    LED_1s      : out   std_logic; -- R[9]: R17
    
    FLASH_ADDR  : out   std_logic_vector(21 downto 0); -- R13,U13,V14,U14,AA20,AB12,AB13,AA13, AB14,T12,R12,Y13,R14,W15,V15,U15, T15,R15,Y16,AA14,AB20
    FLASH_DATA  : inout std_logic_vector(7 downto 0); -- AA19,AB19,AA18,AB18,AA17,AB17,AA16,AB16
    FLASH_OE_N   : out   std_logic;  -- AA15
    FLASH_RST_N   : out   std_logic; -- W14
    FLASH_WE_N : out   std_logic;     -- Y14
    FLASH_CE_N : out   std_logic     -- AB15?
  );
end entity FlashTest;

architecture BEHAVIOR of FlashTest is
  signal count      : integer := 0;
  signal mem        : integer := 0;
  signal state      : integer := 0;
  signal LED_1s_sig : std_logic := '1';
begin
  
  SECOND_BLINK : process(CLK_24M) is
  -- demonstrate use of internal 24MHz clock for driving LEDR9 on 1Hz
  begin
    if (CLK_24M'event and CLK_24M = '1') then
      count <= count + 1;
      if (count = 12000000) then
        LED_1s_sig <= not LED_1s_sig;
        count <= 0;
      end if;
      LED_1s <= LED_1s_sig;
    end if;
  end process SECOND_BLINK;
  
--  -- WRITE
--  FLASH_RST_N <= '1';
--  FLASH_CE_N <= '0';
--  FLASH_OE_N  <= '1';
--  FLASH_WE_N  <= '0';
--  -- select address for LSByte
--  FLASH_ADDR <= (21 downto 2 => '0') & "11";
--  -- turn green LEDs to LSByte data
--  LED_RG(7 downto 0) <= FLASH_DATA;
  
  
  
  MEMORY : process(CLK_24M) is
  -- demonstrate R/W to the board's FLASH
  begin
    FLASH_RST_N <= '1';
    FLASH_CE_N <= '0';
    
    if (CLK_24M'event and CLK_24M = '1') then
      
      case state is
        when 0 =>
          -- select command WRITE
          FLASH_OE_N  <= '1';
          FLASH_WE_N  <= '0';
          -- select address for LSByte
          FLASH_ADDR <= "00000" & std_logic_vector(to_unsigned(mem, 16)) & "0";
          FLASH_DATA <= std_logic_vector(to_unsigned(mem, 16))(7 downto 0);
          
        when 10 =>
          -- select command WRITE
          FLASH_OE_N  <= '1';
          FLASH_WE_N  <= '0';
          -- select address for MSByte
          FLASH_ADDR <= "00000" & std_logic_vector(to_unsigned(mem, 16)) & "1";
          FLASH_DATA <= std_logic_vector(to_unsigned(mem, 16))(15 downto 8);
          
        when 20 =>
          -- select command READ
          FLASH_OE_N  <= '0';
          FLASH_WE_N  <= '1';
          -- select address for LSByte
          FLASH_ADDR <= "00000" & std_logic_vector(to_unsigned(mem, 16)) & "0";
          -- turn green LEDs to LSByte data
          LED_RG(7 downto 0) <= FLASH_DATA;
        when 30 =>
          -- select command READ
          FLASH_OE_N  <= '0';
          FLASH_WE_N  <= '1';
          -- select address for MSByte
          FLASH_ADDR <= "00000" & std_logic_vector(to_unsigned(mem, 16)) & "1";
          -- turn green LEDs to LSByte data
          LED_RG(15 downto 8) <= FLASH_DATA;
          
        when others =>
          -- nop
      end case;
      
      state <= state + 1;
      if (state = 12000000) then
        state <= 0;
        mem <= mem + 1;
        if (mem = 65536) then
          mem <= 0;
        end if;
      end if;
    end if;
    
  end process MEMORY;
end architecture BEHAVIOR;
