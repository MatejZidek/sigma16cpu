
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity MemoryTest is
  port (
    CLK_24M     : in    std_logic; -- A12/B12
    LED_RG      : out   std_logic_vector(15 downto 0); -- R[7]-R[0]+G[7]-G[0]: U18,Y18,V19,T18,Y19,U19,R19,R20 Y21,Y22,W21,W22,V21,V22,U21,U22
    LED_1s      : out   std_logic; -- R[9]: R17
    
    SDRAM_ADDR  : out   std_logic_vector(11 downto 0); -- W4,W5,Y3,Y4,R6,R5,P6,P5,P3,N4,W3,N6
    SDRAM_DATA  : inout std_logic_vector(15 downto 0); -- U1,U2,v1,V2,W1,W2,Y1,Y2,N1,N2,P1,P2,R1,R2,T1,T2
    SDRAM_BANK0 : out   std_logic; -- U3
    SDRAM_BANK1 : out   std_logic; -- V4
    SDRAM_LDQM  : out   std_logic; -- R7
    SDRAM_UDQM  : out   std_logic; -- M5
    SDRAM_RAS_N : out   std_logic; -- T5
    SDRAM_CAS_N : out   std_logic; -- T3
    SDRAM_CKE   : out   std_logic; -- N3
    SDRAM_CLK   : out   std_logic; -- U4
    SDRAM_WE_N  : out   std_logic; -- R8
    SDRAM_CS_N  : out   std_logic  -- T6
  );
end entity MemoryTest;

architecture BEHAVIOR of MemoryTest is
  signal count      : integer := 0;
  signal mem        : integer := 0;
  signal state      : integer := 0;
  signal LED_1s_sig : std_logic := '1';
begin
  
  SECOND_BLINK : process(CLK_24M) is
  -- demonstrate use of internal 24MHz clock for driving LEDR9 on 1Hz
  begin
    if (CLK_24M'event and CLK_24M = '1') then
      count <= count + 1;
      if (count = 12000000) then
        LED_1s_sig <= not LED_1s_sig;
        count <= 0;
      end if;
      LED_1s <= LED_1s_sig;
    end if;
  end process SECOND_BLINK;
  
  MEMORY : process(CLK_24M) is
  -- demonstrate R/W to the board's SDRAM
  begin
    SDRAM_CKE <= '1'; -- clock enable on all the time (no need to stop the clock)
    SDRAM_CLK <= CLK_24M; -- connect SDRAM clock to the 24MHz clock
    SDRAM_CS_N <= '0'; -- chip select active all the time (we are not using any other chip)
    SDRAM_LDQM <= '0'; -- always active
    SDRAM_UDQM <= '0'; -- always active
    SDRAM_BANK0 <= '0'; -- use bank 00 all the time
    SDRAM_BANK1 <= '0'; -- use bank 00 all the time
    
    if (CLK_24M'event and CLK_24M = '1') then
      -- nop
      SDRAM_RAS_N <= '1';
      SDRAM_CAS_N <= '1';
      SDRAM_WE_N  <= '1';
      
      case state is
        when 0 =>
          -- select command ACTIVE
          SDRAM_RAS_N <= '0';
          SDRAM_CAS_N <= '1';
          SDRAM_WE_N  <= '1';
          -- select activate row
          SDRAM_ADDR <= "0000" & std_logic_vector(to_unsigned(mem, 16))(15 downto 8);
          
        when 1 =>
          -- select command WRITE
          SDRAM_RAS_N <= '1';
          SDRAM_CAS_N <= '0';
          SDRAM_WE_N  <= '0';
          -- select column and auto precharge
          SDRAM_ADDR <= "0100" & std_logic_vector(to_unsigned(mem, 16))(7 downto 0);
          -- set data
          SDRAM_DATA <= std_logic_vector(to_unsigned(mem, 16));
          
        when 2 =>
          -- select command ACTIVE
          SDRAM_RAS_N <= '0';
          SDRAM_CAS_N <= '1';
          SDRAM_WE_N  <= '1';
          -- select activate row
          SDRAM_ADDR <= "0000" & std_logic_vector(to_unsigned(mem, 16))(15 downto 8);
        when 3 =>
          -- select command READ
          SDRAM_RAS_N <= '1';
          SDRAM_CAS_N <= '0';
          SDRAM_WE_N  <= '1';
          -- select column, auto precharge
          SDRAM_ADDR <= "0100" & std_logic_vector(to_unsigned(mem, 16))(7 downto 0);
          -- output read data to LED_RG
          LED_RG <= SDRAM_DATA;
          
        when others =>
          -- nop
          SDRAM_RAS_N <= '1';
          SDRAM_CAS_N <= '1';
          SDRAM_WE_N  <= '1';
      end case;
      
      state <= state + 1;
      if (state = 24000000) then
        state <= 0;
        mem <= mem + 1;
        if (mem = 65536) then
          mem <= 0;
        end if;
      end if;
    end if;
    
  end process MEMORY;
end architecture BEHAVIOR;
