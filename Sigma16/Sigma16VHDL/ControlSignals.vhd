-- The behaviour of the datapath is determined by the values of the
-- control signals.  A naming convention is used for these signals.
-- Each control signal has a name with three parts, separated by _.
-- The first part is ctl (to indicate that it's a control signal).
-- The second part is the name of the subsystem in the datapath which
-- is being controlled, and the third part is the specific operation
-- being commanded by the signal.

library ieee;
  use ieee.std_logic_1164.all;

package CONTROL_SIGNALS is

  type ctl_sig is record
    -- Controls for ALU
    ctl_alu_a : std_logic;   -- 4-bit alu operation code (see section on the ALU)
    ctl_alu_b : std_logic;   --   "
    ctl_alu_c : std_logic;   --   "
    ctl_alu_d : std_logic;   --   "
    ctl_x_pc : std_logic;    -- Transmit pc on x (if 0 : std_logic; transmit reg[sa])
    ctl_y_ad : std_logic;    -- Transmit ad on y (if 0 : std_logic; transmit reg[sb])

    -- Controls for register file
    ctl_rf_ld : std_logic;   --  Load  register file (if 0 : std_logic; remain unchanged)
    ctl_rf_pc : std_logic;   --   Input to register file is pc (if 0 : std_logic; check ctl_rf_alu)
    ctl_rf_alu : std_logic;  -- Input to register file is ALU output r (if 0 : std_logic; use m)
    ctl_rf_sd : std_logic;   -- Use ir_d as source a address (if 0 : std_logic; use ir_sa)

    -- Controls for system registers
    ctl_ir_ld : std_logic;   -- Load ir register (if 0 : std_logic; remain unchanged)
    ctl_pc_ld : std_logic;   -- Load pc register (if 0 : std_logic; remain unchanged)
    ctl_pc_ad : std_logic;   -- Input to pc is ad (if 0 : std_logic; r)
    ctl_ad_ld : std_logic;   -- Load ad register (if 0 : std_logic; remain unchanged)
    ctl_ad_alu : std_logic;  -- Obtain ad input from alu (if 0 : std_logic; from memory data input)

    -- Controls for memory
    ctl_ma_pc : std_logic;   -- Transmit pc on memory address bus (if 0 : std_logic; transmit addr)
    ctl_sto: std_logic;      -- Memory store (if 0 : std_logic; fetch)
  end record;

  type ctl_state is (
    st_instr_fet,
    st_dispatch,

    st_add,
    st_sub,
    st_mul0, -- unimplemented
    st_div0, -- unimplemented
    st_cmplt,
    st_cmpeq,
    st_cmpgt,
    st_inv, -- unimplemented
    st_and, -- unimplemented
    st_or, -- unimplemented
    st_xor, -- unimplemented
    st_shiftl, -- unimplemented
    st_shiftr, -- unimplemented
    st_trap0,

    st_lea0,
    st_lea1,

    st_load0,
    st_load1,
    st_load2,

    st_store0,
    st_store1,
    st_store2,

    st_jump0,
    st_jump1,

    st_jumpf0,
    st_jumpf1,

    st_jumpt0,
    st_jumpt1,

    st_jal0,
    st_jal1
  );

  constant RRR_add : std_logic_vector(3 downto 0) := "0000";
  constant RRR_sub : std_logic_vector(3 downto 0) := "0001"; -- unimplemented
  constant RRR_mul : std_logic_vector(3 downto 0) := "0010"; -- unimplemented
  constant RRR_div : std_logic_vector(3 downto 0) := "0011";
  constant RRR_cmplt : std_logic_vector(3 downto 0) := "0100";
  constant RRR_cmpeq : std_logic_vector(3 downto 0) := "0101";
  constant RRR_cmpgt : std_logic_vector(3 downto 0) := "0110";
  constant RRR_inv : std_logic_vector(3 downto 0) := "0111"; -- unimplemented
  constant RRR_and : std_logic_vector(3 downto 0) := "1000"; -- unimplemented
  constant RRR_or : std_logic_vector(3 downto 0) := "1001"; -- unimplemented
  constant RRR_xor : std_logic_vector(3 downto 0) := "1010"; -- unimplemented
  constant RRR_shiftl : std_logic_vector(3 downto 0) := "1011"; -- unimplemented
  constant RRR_shiftr : std_logic_vector(3 downto 0) := "1100"; -- unimplemented
  constant RRR_trap : std_logic_vector(3 downto 0) := "1101";
  constant XX : std_logic_vector(3 downto 0) := "1110";
  constant RX : std_logic_vector(3 downto 0) := "1111";

  constant RX_lea : std_logic_vector(3 downto 0) := "0000";
  constant RX_load : std_logic_vector(3 downto 0) := "0001";
  constant RX_store : std_logic_vector(3 downto 0) := "0010";
  constant RX_jump : std_logic_vector(3 downto 0) := "0011";
  constant RX_jump0 : std_logic_vector(3 downto 0) := "0100"; -- nop
  constant RX_jump1 : std_logic_vector(3 downto 0) := "0101"; -- nop
  constant RX_jumpf : std_logic_vector(3 downto 0) := "0110";
  constant RX_jumpt : std_logic_vector(3 downto 0) := "0111";
  constant RX_jal : std_logic_vector(3 downto 0) := "1000";
  constant RX_9 : std_logic_vector(3 downto 0) := "1001"; -- nop
  constant RX_a : std_logic_vector(3 downto 0) := "1010"; -- nop
  constant RX_b : std_logic_vector(3 downto 0) := "1011"; -- nop
  constant RX_c : std_logic_vector(3 downto 0) := "1100"; -- nop
  constant RX_d : std_logic_vector(3 downto 0) := "1101"; -- nop
  constant RX_e : std_logic_vector(3 downto 0) := "1110"; -- nop
  constant RX_f : std_logic_vector(3 downto 0) := "1111"; -- nop

end package CONTROL_SIGNALS;
