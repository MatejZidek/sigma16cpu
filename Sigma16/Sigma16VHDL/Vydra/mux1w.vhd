
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity MUX1W is
  generic (
    N : integer := 16
  );
  port (
    S : in    std_logic;
    A : in    std_logic_vector(N - 1 downto 0);
    B : in    std_logic_vector(N - 1 downto 0);
    Q : out   std_logic_vector(N - 1 downto 0)
  );
end entity MUX1W;

architecture STRUCTURE of MUX1W is

begin

  Q <= A when S = '0' else
       B;

end architecture STRUCTURE;
