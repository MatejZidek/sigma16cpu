
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity MUX2 is
  port (
    S : in    std_logic_vector(1 downto 0);
    A : in    std_logic;
    B : in    std_logic;
    C : in    std_logic;
    D : in    std_logic;
    Q : out   std_logic
  );
end entity MUX2;

architecture STRUCTURE of MUX2 is

  component MUX1 is
    port (
      S : in    std_logic;
      A : in    std_logic;
      B : in    std_logic;
      Q : out   std_logic
    );
  end component;

  signal q1 : std_logic;
  signal q2 : std_logic;

begin

  MUX_Q1 : MUX1
    port map (
      S => S(0),
      A => A,
      B => B,
      Q => q1
    );

  MUX_Q2 : MUX1
    port map (
      S => S(0),
      A => C,
      B => D,
      Q => q2
    );

  MUX_Q : MUX1
    port map (
      S => S(1),
      A => q1,
      B => q2,
      Q => Q
    );

  --  Q <= A when S = "00" else
  --       B when S = "01" else
  --       C when S = "10" else
  --       D when S = "11" else
  --       (Q'range => '0');

end architecture STRUCTURE;
