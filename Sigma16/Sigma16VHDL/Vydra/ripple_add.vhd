
library ieee;
  use ieee.std_logic_1164.all;

entity RIPPLE_ADD is
  generic (
    N : integer := 16
  );
  port (
    A     : in    std_logic_vector(N - 1 downto 0);
    B     : in    std_logic_vector(N - 1 downto 0);
    C_IN  : in    std_logic;

    S     : out   std_logic_vector(N - 1 downto 0);
    C_OUT : out   std_logic
  );
end entity RIPPLE_ADD;

architecture STRUCTURE of RIPPLE_ADD is

  component FULL_ADD is PORT(
    X: IN std_logic;
    Y: IN std_logic;
    C_IN: IN std_logic;
    SUM: OUT std_logic;
    C_OUT: OUT std_logic);
  end component;

  signal carry : std_logic_vector(n downto 0);

begin

  carry(0) <= C_IN;
  C_OUT    <= carry(n);

  FA : for i in 0 to N - 1 generate

    FA_I : FULL_ADD
      port map (
        X     => carry(i),
        Y     => A(i),
        C_IN  => B(i),
        SUM   => S(i),
        C_OUT => carry(i + 1)
      );

  end generate FA;

end architecture STRUCTURE;
