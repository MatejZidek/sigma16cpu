
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity REGFILE is
  generic (
    N : integer := 16;                            -- word size
    K : integer := 4                              -- the register file contains 2^k registers
  );
  port (
    CLK : in    std_logic;                        -- clock
    LD  : in    std_logic;                        -- load control
    D   : in    std_logic_vector(K - 1 downto 0); -- destination register
    SA  : in    std_logic_vector(K - 1 downto 0); -- source register a
    SB  : in    std_logic_vector(K - 1 downto 0); -- source register b
    X   : in    std_logic_vector(N - 1 downto 0); -- data input

    A   : out   std_logic_vector(N - 1 downto 0); -- reg[sa]
    B   : out   std_logic_vector(N - 1 downto 0)  -- reg[sb]
  );
end entity REGFILE;

architecture BEHAVIOR of REGFILE is

  type register_array is array (0 to 2 ** K - 1) of std_logic_vector(N - 1 downto 0);

  signal reg : register_array := (others => (others => '0'));

begin

  CLOCK : process (CLK) is
  begin

    if (CLK'event and CLK = '1') then
      if (ld = '1') then
        reg(to_integer(unsigned(D))) <= x;
      end if;
      A <= reg(to_integer(unsigned(SA)));
      B <= reg(to_integer(unsigned(SB)));
    end if;

  end process CLOCK;

end architecture BEHAVIOR;
