
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity REG is
  generic (
    N : integer := 16                              -- word size
  );
  port (
    CLK  : in    std_logic;                        -- clock
    LD   : in    std_logic;                        -- load control
    D    : in    std_logic_vector(N - 1 downto 0); -- data input

    Q    : out   std_logic_vector(N - 1 downto 0)  -- data out
  );
end entity REG;

architecture BEHAVIOR of REG is

begin

  CLOCK : process (CLK, LD) is
  begin

    if (CLK'event and CLK = '1') then
      if (LD = '1') then
        Q <= D;
      end if;
    end if;

  end process CLOCK;

end architecture BEHAVIOR;
