
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity MUX2W is
  generic (
    N : integer := 16
  );
  port (
    S : in    std_logic_vector(1 downto 0);
    A : in    std_logic_vector(N - 1 downto 0);
    B : in    std_logic_vector(N - 1 downto 0);
    C : in    std_logic_vector(N - 1 downto 0);
    D : in    std_logic_vector(N - 1 downto 0);
    Q : out   std_logic_vector(N - 1 downto 0)
  );
end entity MUX2W;

architecture STRUCTURE of MUX2W is

  component MUX1W is
    generic (
      N : integer
    );
    port (
      S : in    std_logic;
      A : in    std_logic_vector(N - 1 downto 0);
      B : in    std_logic_vector(N - 1 downto 0);
      Q : out   std_logic_vector(N - 1 downto 0)
    );
  end component;

  signal q1 : std_logic_vector(N - 1 downto 0);
  signal q2 : std_logic_vector(N - 1 downto 0);

begin

  MUX_Q1 : MUX1W
    generic map (
      N => N
    )
    port map (
      S => S(0),
      A => A,
      B => B,
      Q => q1
    );

  MUX_Q2 : MUX1W
    generic map (
      N => N
    )
    port map (
      S => S(0),
      A => C,
      B => D,
      Q => q2
    );

  MUX_Q : MUX1W
    generic map (
      N => N
    )
    port map (
      S => S(1),
      A => q1,
      B => q2,
      Q => Q
    );

  --  Q <= A when S = "00" else
  --       B when S = "01" else
  --       C when S = "10" else
  --       D when S = "11" else
  --       (Q'range => '0');

end architecture STRUCTURE;
