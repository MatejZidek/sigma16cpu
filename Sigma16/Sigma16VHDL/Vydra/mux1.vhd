
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity MUX1 is
  port (
    S : in    std_logic;
    A : in    std_logic;
    B : in    std_logic;
    Q : out   std_logic
  );
end entity MUX1;

architecture DATAFLOW of MUX1 is

begin

  Q <= A when S = '0' else
       B;

end architecture DATAFLOW;
