
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity FULL_ADD is
  port (
    X         : in    std_logic;
    Y         : in    std_logic;
    C_IN      : in    std_logic;
    
    SUM       : out   std_logic;
    C_OUT     : out   std_logic
  );
end entity FULL_ADD;

architecture STRUCTURE of FULL_ADD is

begin

  SUM     <= X xor Y xor C_IN;
  C_out   <= (X and Y) or (C_IN and Y) or (C_IN and X);

end architecture STRUCTURE;
