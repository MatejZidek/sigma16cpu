------------------------------------------------------------------------
--           Datapath
------------------------------------------------------------------------
--The datapath contains the registers, computational systems, and
--interconnections.  It has two inputs: a set of control signals
--provided by the control unit, and a data word from the either the
--memory system or the DMA input controller.

library ieee;
  use ieee.std_logic_1164.all;
  use work.CONTROL_SIGNALS.all;

entity DATAPATH is
  generic (
    N : integer := 16;                                 -- word size
    K : integer := 4                                   -- the register file contains 2^k registers
  );
  port (
    CLK      : in    std_logic;                        -- clock
    CTLSIGS  : in    ctl_sig;                          -- control signals
    MEMDAT   : in    std_logic_vector(N - 1 downto 0); -- data word

    MA       : out   std_logic_vector(N - 1 downto 0); -- memory address
    MD       : out   std_logic_vector(N - 1 downto 0); -- memory data
    COND     : out   std_logic;                        -- boolean for conditionals
    A        : out   std_logic_vector(N - 1 downto 0); -- regfile output 1
    B        : out   std_logic_vector(N - 1 downto 0); -- regfile output 2
    IR       : out   std_logic_vector(N - 1 downto 0); -- instruction
    PC       : out   std_logic_vector(N - 1 downto 0); -- programm counter
    AD       : out   std_logic_vector(N - 1 downto 0); -- address register
    OVFL     : out   std_logic;                        -- ALU overflow
    R        : out   std_logic_vector(N - 1 downto 0); -- ALU result
    X        : out   std_logic_vector(N - 1 downto 0); -- alu input 1
    Y        : out   std_logic_vector(N - 1 downto 0); -- alu input 2
    P        : out   std_logic_vector(N - 1 downto 0)  -- regfile data input
  );
end entity DATAPATH;

architecture STRUCTURE of DATAPATH is

  component REGFILE is
    generic (
      N : integer;
      K : integer
    );
    port (
      CLK : in    std_logic;
      LD  : in    std_logic;
      D   : in    std_logic_vector(K - 1 downto 0);
      SA  : in    std_logic_vector(K - 1 downto 0);
      SB  : in    std_logic_vector(K - 1 downto 0);
      X   : in    std_logic_vector(N - 1 downto 0);

      A   : out   std_logic_vector(N - 1 downto 0);
      B   : out   std_logic_vector(N - 1 downto 0)
    );
  end component;

  component REG is
    generic (
      N : integer
    );
    port (
      CLK  : in    std_logic;
      LD   : in    std_logic;
      D    : in    std_logic_vector(N - 1 downto 0);

      Q    : out   std_logic_vector(N - 1 downto 0)
    );
  end component;

  component ALU is
    generic (
      N : integer
    );
    port (
      A     : in    std_logic;
      B     : in    std_logic;
      C     : in    std_logic;
      D     : in    std_logic;
      X     : in    std_logic_vector(N - 1 downto 0);
      Y     : in    std_logic_vector(N - 1 downto 0);
      C_OUT : out   std_logic;
      Z     : out   std_logic_vector(N - 1 downto 0)
    );
  end component;

  component MUX1W is
    generic (
      N : integer
    );
    port (
      S : in    std_logic;
      A : in    std_logic_vector(N - 1 downto 0);
      B : in    std_logic_vector(N - 1 downto 0);
      Q : out   std_logic_vector(N - 1 downto 0)
    );
  end component;

  signal ma_sig   : std_logic_vector(N - 1 downto 0); -- memory address
  signal md_sig   : std_logic_vector(N - 1 downto 0); -- memory data
  signal cond_sig : std_logic;                        -- boolean for conditionals
  signal a_sig    : std_logic_vector(N - 1 downto 0); -- regfile output 1
  signal b_sig    : std_logic_vector(N - 1 downto 0); -- regfile output 2
  signal ir_sig   : std_logic_vector(N - 1 downto 0);
  signal pc_sig   : std_logic_vector(N - 1 downto 0);
  signal ad_sig   : std_logic_vector(N - 1 downto 0);
  signal ovfl_sig : std_logic;                        -- ALU overflow
  signal r_sig    : std_logic_vector(N - 1 downto 0); -- ALU result
  signal x_sig    : std_logic_vector(N - 1 downto 0); -- alu input 1
  signal y_sig    : std_logic_vector(N - 1 downto 0); -- alu input 2
  signal p_sig    : std_logic_vector(N - 1 downto 0); -- regfile data input

  signal ir_op    : std_logic_vector(K - 1 downto 0); -- instruction opcode
  signal ir_d     : std_logic_vector(K - 1 downto 0); -- instruction destination register
  signal ir_sa    : std_logic_vector(K - 1 downto 0); -- instruction source a register
  signal ir_sb    : std_logic_vector(K - 1 downto 0); -- instruction source b register

  signal rf_sa    : std_logic_vector(K - 1 downto 0); -- a = reg[rf_sa]
  signal rf_sb    : std_logic_vector(K - 1 downto 0); -- b = reg[rf_sb]

  signal p0       : std_logic_vector(N - 1 downto 0); -- regfile data input, first step
  signal q        : std_logic_vector(N - 1 downto 0); -- input to pc
  signal ad_in    : std_logic_vector(N - 1 downto 0); -- input to ad

begin

  REGISTER_FILE : REGFILE
    generic map (
      N => N,
      K => K
    )
    port map (
      CLK => CLK,
      LD  => CTLSIGS.ctl_rf_ld,
      D   => ir_d,
      SA  => rf_sa,
      SB  => rf_sb,
      X   => p_sig,

      A   => a_sig,
      B   => b_sig
    );

  IR_REG : REG
    generic map (
      N => N
    )
    port map (
      CLK => CLK,
      LD  => CTLSIGS.ctl_ir_ld,
      D   => MEMDAT,

      Q   => ir_sig
    );

  PC_REG : REG
    generic map (
      N => N
    )
    port map (
      CLK => CLK,
      LD  => CTLSIGS.ctl_pc_ld,
      D   => q,

      Q   => pc_sig
    );

  AD_REG : REG
    generic map (
      N => N
    )
    port map (
      CLK => CLK,
      LD  => CTLSIGS.ctl_ad_ld,
      D   => ad_in,

      Q   => ad_sig
    );

  AD_IN_MUX : MUX1W
    generic map (
      N => N
    )
    port map (
      S => CTLSIGS.ctl_ad_alu,
      A => MEMDAT,
      B => r_sig,

      Q => ad_in
    );

  CONNECT_ALU : ALU
    generic map (
      N => N
    )
    port map (
      A     => CTLSIGS.ctl_alu_a,
      B     => CTLSIGS.ctl_alu_b,
      C     => CTLSIGS.ctl_alu_c,
      D     => CTLSIGS.ctl_alu_d,
      X     => x_sig,
      Y     => y_sig,

      C_OUT => ovfl_sig,
      Z     => r_sig
    );

  SELECT_X : MUX1W
    generic map (
      N => N
    )
    port map (
      S => CTLSIGS.ctl_x_pc,
      A => a_sig,
      B => pc_sig,

      Q => x_sig
    );

  SELECT_Y : MUX1W
    generic map (
      N => N
    )
    port map (
      S => CTLSIGS.ctl_y_ad,
      A => b_sig,
      B => ad_sig,

      Q => y_sig
    );

  SELECT_RF_SA : MUX1W
    generic map (
      N => K
    )
    port map (
      S => CTLSIGS.ctl_rf_sd,
      A => ir_sa,
      B => ir_d,

      Q => rf_sa
    );

  rf_sb <= ir_sb;

  SELECT_P0 : MUX1W
    generic map (
      N => N
    )
    port map (
      S => CTLSIGS.ctl_rf_alu,
      A => MEMDAT,
      B => r_sig,

      Q => p0
    );

  SELECT_P : MUX1W
    generic map (
      N => N
    )
    port map (
      S => CTLSIGS.ctl_rf_pc,
      A => p0,
      B => pc_sig,

      Q => p_sig
    );

  SELECT_Q : MUX1W
    generic map (
      N => N
    )
    port map (
      S => CTLSIGS.ctl_pc_ad,
      A => r_sig,
      B => ad_sig,

      Q => q
    );

  SELECT_MA : MUX1W
    generic map (
      N => N
    )
    port map (
      S => CTLSIGS.ctl_ma_pc,
      A => ad_sig,
      B => pc_sig,

      Q => ma_sig
    );

  md_sig   <= a_sig;
  cond_sig <= '0' when (a_sig = (a_sig'range => '0')) else
              '1'; -- orw A

  ir_op <= ir_sig(N - 1 downto 3*K);
  ir_d  <= ir_sig(3*K - 1 downto 2*K);
  ir_sa <= ir_sig(2*K - 1 downto K);
  ir_sb <= ir_sig(K - 1 downto 0);

  MA   <= ma_sig;
  MD   <= md_sig;
  COND <= cond_sig;
  A    <= a_sig;
  B    <= b_sig;
  IR   <= ir_sig;
  PC   <= pc_sig;
  AD   <= ad_sig;
  OVFL <= ovfl_sig;
  R    <= r_sig;
  X    <= x_sig;
  Y    <= y_sig;
  P    <= p_sig;

end architecture STRUCTURE;
