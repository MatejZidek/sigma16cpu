
library ieee;
  use ieee.std_logic_1164.all;
-- unfinished
entity M1 is
  port (
    RESET       : in    std_logic;
    DMA         : in    std_logic;
    DMA_A       : in    std_logic_vector(15 downto 0);
    DMA_B       : in    std_logic_vector(15 downto 0);

    CTL_STATE   : out   std_logic;
    CTL_START   : out   std_logic;
    CTLSIGS     : out   std_logic;
    -- datapath_outputs: out <TODO>;
    M_STO       : out   std_logic;
    M_ADDR      : out   std_logic;
    M_REAL_ADDR : out   std_logic;
    M_DATA      : out   std_logic;
    M_OUT       : out   std_logic
  );
end entity M1;

architecture BEHAVIOR of M1 is

begin

end architecture BEHAVIOR;
