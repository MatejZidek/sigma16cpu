# Sigma16 in VHDL

## How to run this project

- download Quartus II Web Edition v13.0sp1 from https://fpgasoftware.intel.com/13.0sp1/?edition=web

Following can be skipped if using without an FPGA board:

- go through "Installation and USB-Blaster Driver"in "Getting Started with Altera DE1.pdf" file located in the "CycloneII_FPGA_Starter_Development_Board" directory
- Windows 10 users will have tu disable driver signature verification: https://www.howtogeek.com/167723/how-to-disable-driver-signature-verification-on-64-bit-windows-8.1-so-that-you-can-install-unsigned-drivers/
- connect the FPGA board to the computer and power supply and turn it on

Back to everybody:

- Quartus II project files end with .qpf, our main project is Sigma16VHDL.qpf, but two more can be found in [Memory] directory
- synthesising the VHDL code an running simulations can done in Quartus without having an FPGA board
- however Quartus is a professional tool and requires significant time to familiarise with it
- "Getting Started with Altera DE1.pdf" file located in the "CycloneII_FPGA_Starter_Development_Board" directory is a good place to start