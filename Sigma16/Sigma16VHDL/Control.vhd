----------------------------------------------------------------------
--        Control Algorithm
----------------------------------------------------------------------
--repeat forever
--  st_instr_fet:
--    ir := mem[pc], pc++;
--       {ctl_ma_pc, ctl_ir_ld, ctl_x_pc, ctl_alu=alu_inc, ctl_pc_ld}
--  st_dispatch:
--  case ir_op of
--
--    0 -> -- add instruction
--        st_add:
--          reg[ir_d] := reg[ir_sa] + reg[ir_sb]
--             {ctl_alu_abcd=0000, ctl_rf_alu, ctl_rf_ld}
--
--    1 -> -- sub instruction
--        st_sub:
--          reg[ir_d] := reg[ir_sa] - reg[ir_sb]
--             {ctl_alu_abcd=0100, ctl_rf_alu, ctl_rf_ld}
--
--    2 -> -- mul instruction
--        -- unimplemented
--
--    3 -> -- div instruction
--        -- unimplemented
--
--    4 -> -- cmplt instruction
--        st_cmplt:
--          reg[ir_d] := reg[ir_sa] < reg[ir_sb]
--            assert [ctl_alu_abcd=1101, ctl_rf_alu, ctl_rf_ld]
--
--    5 -> -- cmpeq instruction
--        st_cmpeq:
--          reg[ir_d] := reg[ir_sa] = reg[ir_sb]
--            assert [ctl_alu_abcd=1110, ctl_rf_alu, ctl_rf_ld]
--
--    6 -> -- cmpgt instruction
--        st_cmpgt:
--          reg[ir_d] := reg[ir_sa] > reg[ir_sb]
--            assert [ctl_alu_abcd=1111, ctl_rf_alu, ctl_rf_ld]
--
--    7 -> -- inv instruction
--        -- unimplemented
--
--    8 -> -- and instruction
--        -- unimplemented
--
--    9 -> -- or instruction
--        -- unimplemented
--
--    10 -> -- xor instruction
--        -- unimplemented
--
--    11 -> -- shiftl instruction
--        -- unimplemented
--
--    12 -> -- shiftr instruction
--        -- unimplemented
--
--    13 -> -- trap instruction
--        st_trap0:
--          -- The trap instruction is not implemented, but the
--          -- simulation driver can detect that a trap has been
--          -- executed by observing when the control algorithm
--          -- enters this state.  The simulation driver can then
--          -- terminate the simulation.
--
--    14 -> -- expand to XX format
--        -- This code allows expansion to a two-word format with
--        -- room for many more opcode values
--        -- unimplemented; there are currently no XX instructions
--
--    15 -> -- expand to RX format
--
--      case ir_sb of
--        0 -> -- lea instruction
--            st_lea0:
--              ad := mem[pc], pc++;
--              assert [ctl_ma_pc, ctl_ad_ld, ctl_x_pc,
--                      ctl_alu_abcd=1100, ctl_pc_ld]
--            st_lea1:
--              reg[ir_d] := reg[ir_sa] + ad
--                assert [ctl_y_ad, ctl_alu=alu_add, ctl_rf_alu,
--                        ctl_rf_ld]
--
--        1 -> -- load instruction
--            st_load0:
--              ad := mem[pc], pc++;
--                assert [ctl_ma_pc, ctl_ad_ld, ctl_x_pc,
--                        ctl_alu_abcd=1100, ctl_pc_ld]
--            st_load1:
--              ad := reg[ir_sa] + ad
--                assert [set ctl_y_ad, ctl_alu_abcd=0000,
--                        set ctl_ad_ld, ctl_ad_alu]
--            st_load2:
--              reg[ir_d] := mem[ad]
--                assert [ctl_rf_ld]
--
--        2 -> -- store instruction
--            st_store0:
--              ad := mem[pc], pc++;
--                assert [ctl_ma_pc, ctl_ad_ld, ctl_x_pc,
--                        ctl_alu_abcd=1100, ctl_pc_ld]
--            st_store1:
--              ad := reg[ir_sa] + ad
--                assert [ctl_y_ad, ctl_alu_abcd=0000,
--                        set ctl_ad_ld, ctl_ad_alu]
--            st_store2:
--              mem[addr] := reg[ir_d]
--                assert [ctl_rf_sd, ctl_sto]
--
--        3 -> --  jump instruction
--            st_jump0:
--              ad := mem[pc], pc++;
--                assert [ctl_ma_pc, ctl_ad_ld, ctl_x_pc,
--                        ctl_alu=alu_inc, ctl_pc_ld]
--            st_jump1:
--              ad := reg[ir_sa] + ad, pc := reg[ir_sa] + ad
--                assert [ctl_y_ad, ctl_alu=alu_add, ctl_ad_ld,
--                        ctl_ad_alu, ctl_pc_ld]
--
--        4 -> --  jumpc0 instruction, currently nop
--        5 -> --  jumpc1 instruction, currently nop
--
--        6 -> --  jumpf instruction  (was 4)
--            st_jumpf0:
--              ad := mem[pc], pc++;
--                assert [ctl_ma_pc, ctl_ad_ld, ctl_x_pc,
--                        ctl_alu_abcd=1100, ctl_pc_ld, ctl_rf_sd]
--              case reg[ir_sa] = 0 of
--                False: -- nothing to do
--                True:
--                  st_jumpf1:
--                    pc := reg[ir_sa] + ad
--                      assert [ctl_y_ad, ctl_alu_abcd=0000, ctl_pc_ld]
--
--        7 -> -- jumpt instruction (was 5)
--            st_jumpt0:
--              ad := mem[pc], pc++;
--                assert [ctl_ma_pc, ctl_ad_ld, ctl_x_pc,
--                        ctl_alu_abcd=1100, ctl_pc_ld, ctl_rf_sd]
--              case reg[ir_sa] = 0 of
--                False:
--                  st_jumpt1:
--                    pc := reg[ir_sa] + ad
--                      assert [ctl_y_ad, ctl_alu_abcd=0000, ctl_pc_ld]
--                True: -- nothing to do
--
--        8 -> -- jal instruction (was 6)
--            st_jal0:
--              ad := mem[pc], pc++;
--                assert [ctl_ma_pc, ctl_ad_ld, ctl_x_pc,
--                        ctl_alu_abcd=1100, ctl_pc_ld]
--            st_jal1:
--              reg[ir_d] := pc, ad := reg[ir_sa] + ad,
--                pc := reg[ir_sa] + ad
--                assert [ctl_rf_ld, ctl_rf_pc, ctl_y_ad,
--                        ctl_alu_abcd=0000,
--                        ctl_ad_ld, ctl_ad_alu, ctl_pc_ld, ctl_pc_ad]
--
--        9 -> -- nop
--        10 -> -- nop
--        11 -> -- nop
--        12 -> -- nop
--        13 -> -- nop
--        14 -> -- nop
--        15 -> -- nop

library ieee;
  use ieee.std_logic_1164.all;
  use work.CONTROL_SIGNALS.all;

entity CONTROL is
  generic (
    N : integer := 16; -- word size
    K : integer := 4   -- the register file contains 2^k registers
  );
  port (
    CLK       : in    std_logic;
    RESET     : in    std_logic;
    IR        : in    std_logic_vector(15 downto 0);
    COND      : in    std_logic;

    CTLSTATE  : out   ctl_state;
    START     : out   std_logic;
    CTLSIGS   : out   ctl_sig
  );
end entity CONTROL;

architecture BEHAVIOR of CONTROL is

  signal ir_op         : std_logic_vector(3 downto 0);
  signal ir_d          : std_logic_vector(3 downto 0);
  signal ir_sa         : std_logic_vector(3 downto 0);
  signal ir_sb         : std_logic_vector(3 downto 0);

  signal current_state : ctl_state;
  signal next_state    : ctl_state;

  signal start_st      : std_logic;

begin

  ir_op <= IR(15 downto 12);
  ir_d  <= IR(11 downto 8);
  ir_sa <= IR(7 downto 4);
  ir_sb <= IR(3 downto 0);

  CLOCK : process (CLK) is
  begin

    if (CLK'event and CLK = '1') then
      current_state <= next_state;
    end if;

  end process CLOCK;

  NEXT_LOGIC : process (current_state, RESET, IR) is
  begin

    if (RESET = '1') then
      next_state <= st_instr_fet;
    else

      case current_state is

        when st_instr_fet =>
          next_state <= st_dispatch;
        when st_dispatch =>

          case ir_op is

            when RRR_add =>
              next_state <= st_add;
            when RRR_sub =>
              next_state <= st_sub;
            when RRR_mul =>
              next_state <= st_mul0;
            when RRR_div =>
              next_state <= st_div0;
            when RRR_cmplt =>
              next_state <= st_cmplt;
            when RRR_cmpeq =>
              next_state <= st_cmpeq;
            when RRR_cmpgt =>
              next_state <= st_cmpgt;
            when RRR_inv =>
              next_state <= st_inv;
            when RRR_and =>
              next_state <= st_and;
            when RRR_or =>
              next_state <= st_or;
            when RRR_xor =>
              next_state <= st_xor;
            when RRR_shiftl =>
              next_state <= st_shiftl;
            when RRR_shiftr =>
              next_state <= st_shiftr;
            when RRR_trap =>
              next_state <= st_trap0;

            when XX =>
              null;

            when RX =>

              case ir_sb is

                when RX_lea =>
                  next_state <= st_lea0;
                when RX_load =>
                  next_state <= st_load0;
                when RX_store =>
                  next_state <= st_store0;
                when RX_jump =>
                  next_state <= st_jump0;
                when RX_jumpf =>
                  next_state <= st_jumpf0;
                when RX_jumpt =>
                  next_state <= st_jumpt0;
                when RX_jal =>
                  next_state <= st_jal0;
                when others =>
                  null;

              end case;

            when others =>
              null;

          end case;

        when st_add =>
          next_state <= st_instr_fet;
        when st_sub =>
          next_state <= st_instr_fet;
        when st_mul0 =>
          null;
        when st_cmplt =>
          next_state <= st_instr_fet;
        when st_cmpeq =>
          next_state <= st_instr_fet;
        when st_cmpgt =>
          next_state <= st_instr_fet;
        when st_trap0 =>
          next_state <= st_instr_fet;

        when st_lea0 =>
          next_state <= st_lea1;
        when st_lea1 =>
          next_state <= st_instr_fet;

        when st_load0 =>
          next_state <= st_load1;
        when st_load1 =>
          next_state <= st_load2;
        when st_load2 =>
          next_state <= st_instr_fet;

        when st_store0 =>
          next_state <= st_store1;
        when st_store1 =>
          next_state <= st_store2;
        when st_store2 =>
          next_state <= st_instr_fet;

        when st_jump0 =>
          next_state <= st_jump1;
        when st_jump1 =>
          next_state <= st_instr_fet;

        when st_jumpf0 =>
          next_state <= st_jumpf1;
        when st_jumpf1 =>
          next_state <= st_instr_fet;

        when st_jumpt0 =>
          next_state <= st_jumpt1;
        when st_jumpt1 =>
          next_state <= st_instr_fet;

        when st_jal0 =>
          next_state <= st_jal1;
        when st_jal1 =>
          next_state <= st_instr_fet;

        when others =>
          null;

      end case;

    end if;

  end process NEXT_LOGIC;

  -- OUTPUT LOGIC

  CTLSTATE <= current_state;

  with current_state select
  start_st <=
              '1' when st_load2,
              '1' when st_lea1,
              '1' when st_add,
              '1' when st_sub,
              '1' when st_mul0,
              '1' when st_store2,
              '1' when st_cmpeq,
              '1' when st_cmplt,
              '1' when st_cmpgt,
              '1' when st_jumpt1,
              not COND when st_jumpt0,
              '1' when st_jumpf1,
              COND when st_jumpf0,
              '1' when st_jump1,
              '1' when st_jal1,
              '1' when st_trap0,
              '0' when others;
  START    <= RESET or start_st;

  -- Controls for ALU
  with current_state select
  CTLSIGS.ctl_alu_a <=
                       '1' when st_instr_fet,
                       '1' when st_load0,
                       '1' when st_store0,
                       '1' when st_lea0,
                       '1' when st_cmpeq,
                       '1' when st_cmplt,
                       '1' when st_cmpgt,
                       '1' when st_jumpf0,
                       '1' when st_jal0,
                       '0' when others;
  with current_state select
  CTLSIGS.ctl_alu_b <=
                       '1' when st_instr_fet,
                       '1' when st_load0,
                       '1' when st_store0,
                       '1' when st_lea0,
                       '1' when st_sub,
                       '1' when st_cmpeq,
                       '1' when st_cmplt,
                       '1' when st_cmpgt,
                       '1' when st_jumpf0,
                       '1' when st_jal0,
                       '0' when others;
  with current_state select
  CTLSIGS.ctl_alu_c <=
                       '1' when st_cmpeq,
                       '1' when st_cmpgt,
                       '0' when others;
  with current_state select
  CTLSIGS.ctl_alu_d <=
                       '1' when st_cmpeq,
                       '1' when st_cmplt,
                       '1' when st_cmpgt,
                       '0' when others;

  -- Controls for register file
  with current_state select
  CTLSIGS.ctl_x_pc <=
                      '1' when st_instr_fet,
                      '1' when st_load0,
                      '1' when st_lea0,
                      '1' when st_store0,
                      '1' when st_jumpt0,
                      '1' when st_jumpf0,
                      '1' when st_jump0,
                      '1' when st_jal0,
                      '0' when others;
  with current_state select
  CTLSIGS.ctl_y_ad <=
                      '1' when st_load1,
                      '1' when st_store1,
                      '1' when st_lea1,
                      '1' when st_jumpt1,
                      '1' when st_jumpf1,
                      '1' when st_jump1,
                      '1' when st_jal1,
                      '0' when others;
  with current_state select
  CTLSIGS.ctl_rf_ld <=
                       '1' when st_load2,
                       '1' when st_lea1,
                       '1' when st_add,
                       '1' when st_sub,
                       '1' when st_cmpeq,
                       '1' when st_cmplt,
                       '1' when st_cmpgt,
                       '1' when st_jal1,
                       '0' when others;
  with current_state select
  CTLSIGS.ctl_rf_pc <=
                       '1' when st_jal1,
                       '0' when others;
  with current_state select
  CTLSIGS.ctl_rf_alu <=
                        '1' when st_lea1,
                        '1' when st_add,
                        '1' when st_sub,
                        '1' when st_cmpeq,
                        '1' when st_cmplt,
                        '1' when st_cmpgt,
                        '0' when others;
  with current_state select
  CTLSIGS.ctl_rf_sd <=
                       '1' when st_store2,
                       '1' when st_jumpf0,
                       '0' when others;

  -- Controls for system registers
  with current_state select
  CTLSIGS.ctl_ir_ld <=
                       '1' when st_instr_fet,
                       '0' when others;
  with current_state select
  CTLSIGS.ctl_pc_ld <=
                       '1' when st_instr_fet,
                       '1' when st_load0,
                       '1' when st_lea0,
                       '1' when st_store0,
                       '1' when st_jumpt0,
                       '1' when st_jumpt1,
                       '1' when st_jumpf0,
                       '1' when st_jumpf1,
                       '1' when st_jump0,
                       '1' when st_jump1,
                       '1' when st_jal0,
                       '1' when st_jal1,
                       '0' when others;
  with current_state select
  CTLSIGS.ctl_pc_ad <=
                       '1' when st_jal1,
                       '0' when others;
  with current_state select
  CTLSIGS.ctl_ad_ld <=
                       '1' when st_load0,
                       '1' when st_load1,
                       '1' when st_lea0,
                       '1' when st_store0,
                       '1' when st_store1,
                       '1' when st_jumpt0,
                       '1' when st_jumpf0,
                       '1' when st_jump0,
                       '1' when st_jump1,
                       '1' when st_jal0,
                       '1' when st_jal1,
                       '0' when others;
  with current_state select
  CTLSIGS.ctl_ad_alu <=
                        '1' when st_load1,
                        '1' when st_store1,
                        '1' when st_jump1,
                        '1' when st_jal1,
                        '0' when others;

  -- Controls for memory
  with current_state select
  CTLSIGS.ctl_ma_pc <=
                       '1' when st_instr_fet,
                       '1' when st_load0,
                       '1' when st_lea0,
                       '1' when st_store0,
                       '1' when st_jumpt0,
                       '1' when st_jumpf0,
                       '1' when st_jump0,
                       '1' when st_jal0,
                       '0' when others;
  with current_state select
  CTLSIGS.ctl_sto <=
                     '1' when st_store2,
                     '0' when others;

end architecture BEHAVIOR;
