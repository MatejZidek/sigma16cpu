------------------------------------------------------------------------
--			Arithmetic/Logic Unit
------------------------------------------------------------------------
--The ALU performs addition, subtraction, negation, increment, and
--comparision for <, =, and >.  It receives four control signals
--(a,b,c,d) that determine which function it will compute.  These are
--shown in the following table, where don't-care values are left blank.
--
--  |~~~~~~~~~~~~~~~~~~~~~~~~|
--  | Function |  a,b   c,d  |
--  |~~~~~~~~~~|~~~~~~~~~~~~~|
--  |   x+y    |  0 0        |
--  |   x-y    |  0 1        |
--  |    -x    |  1 0        |
--  |   x+1    |  1 1   0 0  |
--  |   x<y    |  1 1   0 1  |
--  |   x=y    |  1 1   1 0  |
--  |   x>y    |  1 1   1 1  |
--  ~~~~~~~~~~~~~~~~~~~~~~~~~~
--
--The control algorithm defines all control signals, regardless of what
--operation is being performed.  The signal values for the ALU are:
--
--  alu_add = 0000
--  alu_sub = 0100
--  alu_neg = 1000
--  alu_inc = 1100
--  alu_lt  = 1101
--  alu_eq  = 1110
--  alu_gt  = 1111

library ieee;
  use ieee.std_logic_1164.all;

entity ALU is
  generic (
    N : integer := 16
  );
  port (
    A     : in    std_logic;
    B     : in    std_logic;
    C     : in    std_logic;
    D     : in    std_logic;
    X     : in    std_logic_vector(N - 1 downto 0);
    Y     : in    std_logic_vector(N - 1 downto 0);

    C_OUT : out   std_logic;
    Z     : out   std_logic_vector(N - 1 downto 0)
  );
end entity ALU;

architecture STRUCTURE of ALU is

  component MUX1W is
    generic (
      N : integer
    );
    port (
      S : in    std_logic;
      A : in    std_logic_vector(N - 1 downto 0);
      B : in    std_logic_vector(N - 1 downto 0);
      Q : out   std_logic_vector(N - 1 downto 0)
    );
  end component;

  component MUX2W is
    generic (
      N : integer
    );
    port (
      S : in    std_logic_vector(1 downto 0);
      A : in    std_logic_vector(N - 1 downto 0);
      B : in    std_logic_vector(N - 1 downto 0);
      C : in    std_logic_vector(N - 1 downto 0);
      D : in    std_logic_vector(N - 1 downto 0);
      Q : out   std_logic_vector(N - 1 downto 0)
    );
  end component;
  
  component MUX2 is
    port (
      S : in    std_logic_vector(1 downto 0);
      A : in    std_logic;
      B : in    std_logic;
      C : in    std_logic;
      D : in    std_logic;
      Q : out   std_logic
    );
  end component;

  component RIPPLE_ADD is
    generic (
      N : integer
    );
    port (
      A     : in    std_logic_vector(N - 1 downto 0);
      B     : in    std_logic_vector(N - 1 downto 0);
      C_IN  : in    std_logic;

      S     : out   std_logic_vector(N - 1 downto 0);
      C_OUT : out   std_logic
    );
  end component;

  constant wzero : std_logic_vector(N - 1 downto 0) := (others => '0');
  constant wone  : std_logic_vector(N - 1 downto 0) := (N - 1 downto 1 => '0') & '1';

  signal negating  : std_logic;
  signal comparing : std_logic;

  signal x1        : std_logic_vector(N - 1 downto 0);
  signal y0        : std_logic_vector(N - 1 downto 0);
  signal y1        : std_logic_vector(N - 1 downto 0);
  signal sum       : std_logic_vector(N - 1 downto 0);

  signal lt        : std_logic;
  signal eq        : std_logic;
  signal gt        : std_logic;
  signal lt_tc     : std_logic;
  signal eq_tc     : std_logic;
  signal gt_tc     : std_logic;
  
  signal comp_bool : std_logic;
  signal comp_word : std_logic_vector(N - 1 downto 0);

begin

  negating  <= a xor b;
  comparing <= a and b and (c or d);

  CHOOSE_X : MUX2W
    generic map (
      N => N
    )
    port map (
      S => A & B,
      A => X,
      B => X,
      C => wzero,
      D => X,
      Q => x1
    );

  CHOOSE_Y0 : MUX1W
    generic map (
      N => N
    )
    port map (
      S => C or D,
      A => wone,
      B => Y,
      Q => y0
    );

  CHOOSE_Y1 : MUX2W
    generic map (
      N => N
    )
    port map (
      S => A & B,
      A => Y,
      B => not Y,
      C => not X,
      D => y0,
      Q => y1
    );

  ADD : RIPPLE_ADD
    generic map (
      N => N
    )
    port map (
      A     => x1,
      B     => y1,
      C_IN  => negating,
      S     => sum,
      C_OUT => C_OUT
    );

  lt <= '1' when x1 < y1 else
        '0';
  eq <= '1' when x1 = y1 else
        '0';
  gt <= '1' when x1 > y1 else
        '0';
        
  SIGNED_LT : MUX2
    port map (
      S => x1(N - 1) & y1(N - 1),
      A => lt,
      B => '0',
      C => '1',
      D => lt,
      Q => lt_tc
    );
  
  eq_tc <= eq;
  
  SIGNED_GT : MUX2
    port map (
      S => x1(N - 1) & y1(N - 1),
      A => gt,
      B => '1',
      C => '0',
      D => gt,
      Q => gt_tc
    );
    
  PICK_CMP : MUX2
    port map (
      S => c & d,
      A => '0',
      B => lt_tc,
      C => eq_tc,
      D => gt_tc,
      Q => comp_bool
    );
  
  comp_word <= (N - 1 downto 1 => '0') & comp_bool;
  
  PICK_Z : MUX1W
    generic map (
      N => N
    )
    port map (
      S => comparing,
      A => sum,
      B => comp_word,
      Q => Z
    );
  
end architecture STRUCTURE;
