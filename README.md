# Sigma16 CPU

## Repository Structure:

- [CycloneII_FPGA_Starter_Development_Board] - manuals, tutorials, datasheets and Control Panel for the FPGA board, author: manufacturers (Altera...)
- [Presentation] - slides for the project presentation
- **[Sigma16]** - code
    - [Hydra-2.3.0] - Hydra source code, author: John T. O'Donnel
    - [Sigma16-2.4.7] - Sigma16 source files including M1 circuit, author: John T. O'Donnel
    - **[Sigma16VHDL]** - VHDL implementation of Sigma16 architecture
        - [Memory]
            - [FlashTest] - Quartus project attempting to use the Flash Memory
            - **[SDRAMtest]** - Quartus project using the SDRAM and Clock
        - **[Vydra]** - components for the system
        - Sigma16VHDL.qpf - main Quartus project
        - ***.vhd** - VHDL files (ALU, Control, ControlSignals, Datapath)
        - **Sim-*.vwf** - Quartus simulator files
- [Sigma16-dissertation] - the dissertation subrepository
- [Status report] - midway status report, timelog
